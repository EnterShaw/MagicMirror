import React, { Component } from 'react'
import moment from 'moment'
import "../css/DateTime.css"

class DateTime extends Component {

    constructor(props) {
        super(props)
        this.state = { time: moment() }
    }

    tick = () =>
        this.setState((prevState) => (
            {
                time: moment()
            }
        ))


    componentDidMount = () => {
        this.interval = setInterval(this.tick, 60000);
    }

    render = () => (
            <div className="datetime">
                <h2>
                    {this.state.time.format("dddd Do MMMM YYYY")}
                </h2>
                <h1>
                    {this.state.time.format("HH:mm")}
                </h1>
            </div>
        );
    
}

export default DateTime;