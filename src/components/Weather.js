import React, { Component } from 'react'
import "../css/Weather.css"
import WeatherIcon from './WeatherIcon.js'
import moment from 'moment'

class Weather extends Component {

    constructor(props) {
        super(props)
        this.state = { weather: [] }
    }

    componentDidMount = () => {
        this.interval = setInterval(this.refresh, 60 * 1000)
        this.refresh()
    }

    refresh = () => {
        fetch("http://datapoint.metoffice.gov.uk/public/data/val/wxfcs/all/json/354286?res=3hourly&key=9e36a01b-0abe-4994-aa1f-307b441fe0c1",
            { method: 'GET' }
        )
            .then(response => response.json())
            .then(json => this.process(json))
            .then(weather => this.setState({ weather }))
    }

    process = json => {
        const nowMinutes = moment().hour() * 60
        if (nowMinutes < 1080)
            return json.SiteRep.DV.Location.Period[0].Rep
                .filter(forecast => ["540", "720", "900", "1080"].includes(forecast.$))
                .filter(forecast => forecast.$ > nowMinutes) //only want future
        else
            return json.SiteRep.DV.Location.Period[1].Rep
                .filter(forecast => ["540", "720", "900", "1080"].includes(forecast.$))
    }

    render = () => {
        const tomorrow = moment().hour() * 60 >= 1080;
        return (
            <div className="weather-base">
                {tomorrow ? <div style={{ textAlign: "center" }}>Tomorrow:</div> : <div />}
                {this.state.weather.map(forecast => (
                    <div key={forecast.$} className="weather-row">
                        <WeatherIcon code={parseInt(forecast.W, 10)} />
                        <div className="weather-tt">
                            <div className="weather-time">{forecast.$ / 60}:00</div>
                            <div className="weather-temp">{forecast.T}&deg;C({forecast.F}&deg;C)</div>
                        </div>
                    </div>
                ))}
            </div>
        )
    }
}

export default Weather;