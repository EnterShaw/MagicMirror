import React, { Component } from 'react'
import '../css/Arrivals.css'
import _ from 'lodash'

class Arrivals extends Component {

    constructor(props) {
        super(props)
        this.state = { arrivals: [] }
    }

    refresh = () => {
        fetch("https://api.tfl.gov.uk/Line/piccadilly/Arrivals/940GZZLUTPN?direction=inbound&app_id=29c649f8&app_key=e5b0fa0998a7af09feb958045ed3570d",
            { method: 'GET' }
        )
            .then(response => response.json())
            .then(json => this.timeToStation(json))
            .then(arrivals => this.setState({ arrivals }))
    }

    timeToStation = json => {
        const times = json.map(response => response.timeToStation)
            .sort((a, b) => a - b)
        const minutes = times.map(seconds => Math.round(seconds / 60))
        const positions = times
            .map(time => Math.round(time * 100 / 1020))
            .reduce((sooner, current) => {
                if (sooner === [])
                    return [current]
                const previous = sooner[sooner.length - 1]
                if (current < previous + 10)
                    return [...sooner, previous + 10]
                else
                    return [...sooner, current]
            }, []);
        return _.zipWith(minutes, positions, (minute, position) =>
            ({ minute, position })
        )
    }


    componentDidMount = () => {
        this.interval = setInterval(this.refresh, 10 * 1000);
        this.refresh();
    }

    render = () =>
        <div className="arrivals-base">
            {this.state.arrivals.map((arrival, index) =>
                <div key={index} style={{ position: "absolute", bottom: arrival.position + "%" }}>
                    {arrival.minute}m
                </div>
            )}
        </div>


}

export default Arrivals;