import React, { Component } from 'react'
import '../css/Tfl.css'

const LineStatus = props => {
    const { status } = props
    return (
        <div>
            <div className="severity-cell">
                {status.severity}
            </div>
            <div className="description-cell">
                {status.reason ? <span> {status.reason} </span> : <span />}
            </div>
        </div>)
}

class Tfl extends Component {

    constructor(props) {
        super(props)
        this.state = { lineData: [] }
        this.lines = ["Circle", "Piccadilly", "Victoria", "Hammersmith & City", "Metropolitan", "Central"]
    }

    refresh = () => {
        fetch("https://api.tfl.gov.uk/Line/Mode/tube/Status?detail=true&app_id=29c649f8&app_key=e5b0fa0998a7af09feb958045ed3570d",
            { method: 'GET' }
        )
            .then(response => response.json())
            .then(json => this.setLines(json))
    }

    setLines = tflResponse => {
        const lineData = tflResponse.filter(line => this.lines.includes(line.name))
            .map(line => {
                const statusesWithDuplicates = line.lineStatuses.map(this.getLineStatus)
                const statuses = statusesWithDuplicates.reduce(this.removeDuplicates, [])
                return { name: line.name, statuses }
            })
        this.setState({ lineData })
    }

    getLineStatus = lineStatus => {
        const severity = lineStatus.statusSeverityDescription
        if (severity !== "Good Service") {
            const reason = lineStatus.reason.split(":")[1]
            return { severity, reason }
        } else {
            return { severity, reason: "" }
        }
    }

    removeDuplicates = (statuses, status) => {
        const index = statuses.findIndex(s => s.reason === status.reason)
        if (index < 0 ) { //no duplicates
            return [...statuses, status]
        } else { 
            const duplicate = statuses[index]
            duplicate.severity = duplicate.severity + "\n" + status.severity
            return [...statuses]
        }
    }

    componentDidMount = () => {
        this.interval = setInterval(this.refresh, 10 * 1000);
        this.refresh();
    }

    render = () => (
        <div className="tfl-base">
            {this.state.lineData.map((line) => {
                const fontColour = line.statuses[0].severity === "Good Service" ?
                    "tfl-row-good" : "tfl-row"
                return (
                    <div className={fontColour} key={line.name}>
                        <div className="tfl-line-cell">{line.name}</div>
                        <div className="tfl-status-cell">
                            {line.statuses.map(
                                (status, index) => <LineStatus status={status} key={index} />
                            )}
                        </div>
                    </div>
                )
            }
            )}
        </div>
    );

}

export default Tfl;