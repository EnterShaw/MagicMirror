import React, { Component } from 'react'
import "../css/WeatherIcons.css"
import sun from "../img/sun.svg"
import cloud from "../img/cloud.png"
import rain from "../img/rain.svg"
import snow from "../img/snow.svg"
import bolt from "../img/bolt.svg"


const icons = [
    {
        codes: [0, 1],
        html: <img src={sun} alt="sun" className="icon-sun" />
    },
    {
        codes: [2, 3],
        html: (
            <div>
                <img src={sun} alt="sun" className="icon-sun-small" />
                <img src={cloud} alt="cloud" style={{ width: "80%", position: "absolute", bottom: "5%", right: "5%" }} />
            </div>
        )
    },
    {
        codes: [5, 6, 7, 8],
        html: <img src={cloud} alt="cloud" style={{ width: "90%", position: "absolute", top: "5%", left: "5%" }} />
    },
    {
        codes: [9, 10, 11, 12],
        html: (
            <div>
                <img src={rain} alt="drop" className="icon-drop4" />
                <img src={rain} alt="drop" className="icon-drop5" />
                <img src={cloud} alt="cloud" className="icon-cloud-small" />
            </div>
        )
    },
    {
        codes: [13, 14, 15],
        html: (
            <div>
                <img src={rain} alt="drop" className="icon-drop1" />
                <img src={rain} alt="drop" className="icon-drop2" />
                <img src={rain} alt="drop" className="icon-drop3" />
                <img src={cloud} alt="cloud" className="icon-cloud-small" />
            </div>
        )
    },
    {
        codes: [16, 17, 18, 19, 20, 21],
        html: (
            <div>
                <img src={snow} alt="drop" className="icon-drop4" />
                <img src={rain} alt="drop" className="icon-drop5" />
                <img src={cloud} alt="cloud" className="icon-cloud-small" />
            </div>
        )
    },
    {
        codes: [22, 23, 24, 25, 26, 27],
        html: (
            <div>
                <img src={snow} alt="drop" className="icon-drop4" />
                <img src={snow} alt="drop" className="icon-drop5" />
                <img src={cloud} alt="cloud" className="icon-cloud-small" />
            </div>
        )
    },
    {
        codes: [28, 29, 30],
        html: (
            <div>
                {<img src={bolt} alt="drop" className="icon-flash" />}
                <img src={cloud} alt="cloud" className="icon-cloud-small" />
            </div>
        )
    }
]

class WeatherIcon extends Component {
    render = () => {
        const { code } = this.props
        return (
            <div className="weather-icon">
                {icons.find(icon => icon.codes.includes(code))
                    .html}
            </div>
        )
    }

}

export default WeatherIcon;