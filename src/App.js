import React, { Component } from 'react'
import './css/App.css'
import DateTime from './components/DateTime'
import Tfl from './components/Tfl'
import Weather from './components/Weather'
import Arrivals from './components/Arrivals'

class App extends Component {
  
  render() {
    return (
      <div className="App">
        <DateTime/>
        <Tfl/>
        <Weather/>
        <Arrivals/>
      </div>
    );
  }
}

export default App;